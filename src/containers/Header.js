import React from "react";
import { Navbar, Button } from "react-bootstrap";
import { connect } from "react-redux";

import * as actions from "../actions";

class Header extends React.Component {
  render() {
    const { login, logout, isAuthenticated } = this.props;
    return (
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/">Apps</a>
          </Navbar.Brand>
        </Navbar.Header>
        <div style={{ float: "right" }}>
          {!isAuthenticated ? (
            <Button bsStyle="primary" style={{ marginTop: 8 }} onClick={login}>
              Login
            </Button>
          ) : (
            <Button bsStyle="primary" style={{ marginTop: 8 }} onClick={logout}>
              Logout
            </Button>
          )}
        </div>
      </Navbar>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.token !== null
});

const mapDispatchToProps = dispatch => ({
  login: () => dispatch(actions.authLogin()),
  logout: () => dispatch(actions.authLogout())
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
