import React from "react";
import { connect } from "react-redux";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import * as actions from "../actions";
import Dashboard from "../pages/Dashbord";
import Pinterest from "../pages/Pinterest";

class App extends React.Component {
  componentDidMount() {
    this.props.autoSignIn();
  }
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/pinterest/:username/:board" component={Pinterest} />
          <Route path="" component={Dashboard} />
          <Redirect to="/" />
        </Switch>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.token !== null
});

const mapDispatchToProps = dispatch => ({
  autoSignIn: () => dispatch(actions.authAutoLogin())
});
export default connect(mapStateToProps, mapDispatchToProps)(App);
