import * as actionTypes from "./actionTypes";

export const authLogin = () => ({
  type: actionTypes.AUTH_LOGIN
});

export const authLoginSuccess = (token, scope) => ({
  type: actionTypes.AUTH_LOGIN_SUCCESS,
  payload: {
    token,
    scope
  }
});

export const authLoginFailure = error => ({
  type: actionTypes.AUTH_LOGIN_FAILURE,
  payload: {
    error
  }
});

export const authLogout = () => ({
  type: actionTypes.AUTH_LOGOUT
});

export const authLogoutSuccess = () => ({
  type: actionTypes.AUTH_LOGOUT_SUCCESS
});

export const authAutoLogin = () => ({
  type: actionTypes.AUTH_AUTO_LOGIN
});
