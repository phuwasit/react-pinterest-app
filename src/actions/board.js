import * as actionTypes from "./actionTypes";

export const getBoard = (username, board) => ({
  type: actionTypes.GET_BOARD,
  payload: {
    username,
    board
  }
});

export const getBoardSuccess = data => ({
  type: actionTypes.GET_BOARD_SUCCESS,
  payload: {
    data
  }
});

export const getBoardFailure = error => ({
  type: actionTypes.GET_BOARD_FAILURE,
  payload: {
    error
  }
});
