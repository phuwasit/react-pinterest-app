import React from "react";
import { Link } from "react-router-dom";
import { Grid, Row, Col } from "react-bootstrap";

import Header from "../containers/Header";

import "./Dashboard.css";
import pinterestLogo from "../assets/images/pinterest_logo.png";

class Dashboard extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Grid>
          <Row className="show-grid">
            <Col xs={8} md={3}>
              <Link to="/pinterest/worldforests/wooded-areas">
                <div className="Card">
                  <img
                    src={pinterestLogo}
                    style={{ width: 100, height: 100 }}
                    alt=""
                  />
                  <h3>Pinterest</h3>
                  <p>username: worldforests</p>
                  <p>board: wooded-areas</p>
                </div>
              </Link>
            </Col>
            <Col xs={8} md={3}>
              <Link to="/pinterest/TouchBKK/euro">
                <div className="Card">
                  <img
                    src={pinterestLogo}
                    style={{ width: 100, height: 100 }}
                    alt=""
                  />
                  <h3>Pinterest</h3>
                  <p>username: TouchBKK</p>
                  <p>board: euro</p>
                </div>
              </Link>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default Dashboard;
