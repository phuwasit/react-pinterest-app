import React from "react";
import { connect } from "react-redux";
import Masonry from "react-masonry-component";

import * as actions from "../actions";

import Card from "../components/Card";

class Pinterest extends React.Component {
  constructor(props) {
    super(props);
    this._renderItems = this._renderItems.bind(this);
  }

  componentDidMount() {
    const { username, board } = this.props.match.params;
    this.props.getBoard(username, board);
  }

  _renderItems() {
    return this.props.items.map((item, index) => (
      <Card key={index} item={item} />
    ));
  }

  render() {
    return (
      <div>
        <Masonry
          className={"my-gallery-class"}
          disableImagesLoaded={false}
          updateOnEachImageLoad={false}
        >
          {this._renderItems()}
        </Masonry>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: state.board.data
  };
};

const mapDispatchToProp = dispatch => {
  return {
    getBoard: (username, board) => dispatch(actions.getBoard(username, board))
  };
};
export default connect(mapStateToProps, mapDispatchToProp)(Pinterest);
