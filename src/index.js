import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import configureStore from "./store/configureStore";
import rootSaga from "./saga";
import App from "./containers/App";
import registerServiceWorker from "./registerServiceWorker";

const store = configureStore();
store.runSaga(rootSaga);

const app = (
  <Provider store={store}>
    <App />
  </Provider>
);

ReactDOM.render(app, document.getElementById("root"));
registerServiceWorker();
