import * as actionTypes from "../actions/actionTypes";

const initialState = {
  token: null,
  scope: null,
  error: null,
  loading: false
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_LOGIN:
      return {
        ...state,
        error: null,
        loading: true
      };
    case actionTypes.AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        token: action.payload.token,
        scope: action.payload.scope,
        error: null,
        loading: false
      };
    case actionTypes.AUTH_LOGIN_FAILURE:
      return {
        ...state,
        token: null,
        scope: null,
        error: action.payload.error,
        loading: false
      };
    case actionTypes.AUTH_LOGOUT:
      return {
        ...state,
        token: null,
        scope: null,
        userId: null
      };
    default:
      return state;
  }
};

export default authReducer;
