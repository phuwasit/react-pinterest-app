import * as actionTypes from "../actions/actionTypes";

const initialState = {
  data: [],
  error: null,
  loading: false
};

const boardReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_BOARD:
      return {
        ...state,
        error: null,
        loading: true
      };
    case actionTypes.GET_BOARD_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        error: null,
        loading: false
      };
    case actionTypes.GET_BOARD_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false
      };
    default:
      return state;
  }
};

export default boardReducer;
