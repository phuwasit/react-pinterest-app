import { all } from "redux-saga/effects";

import { watchAuth } from "./auth";
import { watchBoard } from "./board";

export default function* rootSaga() {
  yield all([watchAuth(), watchBoard()]);
}
