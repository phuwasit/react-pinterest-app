import axios from "axios";
import { takeEvery, all, put } from "redux-saga/effects";

import * as actionTypes from "../actions/actionTypes";
import * as actions from "../actions";

function* getBoard(action) {
  try {
    const username = action.payload.username;
    const board = action.payload.board;
    const response = yield axios.get(
      `https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.pinterest.com%2F${username}%2F${board}.rss%2F`
    );
    console.log(response);
    const data = response.data.items;
    yield put(actions.getBoardSuccess(data));
  } catch (error) {
    yield put(actions.getBoardFailure(error));
  }
}

export function* watchBoard() {
  yield all([takeEvery(actionTypes.GET_BOARD, getBoard)]);
}
