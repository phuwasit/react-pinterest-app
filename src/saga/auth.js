import { takeEvery, all, put, call } from "redux-saga/effects";

import * as actionTypes from "../actions/actionTypes";
import * as actions from "../actions";
import * as services from "../services/pinterest";

function* login(action) {
  try {
    const response = yield call(services.login);
    if (
      response.session &&
      response.session.accessToken &&
      response.session.scope
    ) {
      const accessToken = response.session.accessToken;
      const scope = response.session.scope;
      yield call([localStorage, "setItem"], "token", accessToken);
      yield call([localStorage, "setItem"], "scope", scope);
      yield put(actions.authLoginSuccess(accessToken, scope));
    } else {
      yield put(actions.authLoginFailure(response));
    }
  } catch (error) {
    yield put(actions.authLoginFailure(error));
  }
}

function* logout(action) {
  yield call([localStorage, "removeItem"], "token");
  yield call([localStorage, "removeItem"], "scope");
  yield call(services.logout);
  yield put(actions.authLogoutSuccess());
}

function* autoLogin(action) {
  // const session = yield call(services.getSession);
  const accessToken = yield localStorage.getItem("token");
  const scope = yield localStorage.getItem("scope");
  if (accessToken && scope) {
    yield put(actions.authLoginSuccess(accessToken, scope));
  } else {
    yield put(actions.authLogout());
  }
}

export function* watchAuth() {
  yield all([takeEvery(actionTypes.AUTH_LOGIN, login)]);
  yield all([takeEvery(actionTypes.AUTH_LOGOUT, logout)]);
  yield all([takeEvery(actionTypes.AUTH_AUTO_LOGIN, autoLogin)]);
}
