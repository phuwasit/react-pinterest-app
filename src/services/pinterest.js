export const login = () =>
  new Promise((resolve, reject) => {
    window.PDK.login(
      {
        scope: "read_public"
      },
      function(response) {
        resolve(response);
      },
      function(error) {
        reject(error);
      }
    );
  });

export const logout = () =>
  new Promise((resolve, reject) => {
    resolve(window.PDK.logout());
  });

export const getSession = () =>
  new Promise((resolve, reject) => {
    const session = window.PDK.getSession();
    resolve(session);
  });
