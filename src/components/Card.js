import React from "react";

import "./Card.css";

const Card = ({ item }) => {
  return (
    <div className="card">
      <img src={item.thumbnail} alt={item.title} />
      <p>{item.title}</p>
    </div>
  );
};

export default Card;
